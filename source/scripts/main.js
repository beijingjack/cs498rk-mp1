console.log("Hello World!");

// Sticky nav bar starts
/////////////////////////////////////////
var placePos;
var foodPos;
var sportPos;
var place = document.getElementById("placelink");
var food = document.getElementById("foodlink");
var sport = document.getElementById("sportlink");
window.onscroll = function () {stickyFunction();highlight();}
var navbar = document.getElementById("navbar");
var threeCols = document.getElementById("threeCols");
var sticky = navbar.offsetTop;

function stickyFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky");
        navbar.style.fontSize='16px';
        threeCols.style.paddingTop = navbar.offsetHeight.toString()+'px';

    } else {
        navbar.classList.remove("sticky");
        threeCols.style.paddingTop = '0px';
        navbar.style.fontSize='20px';

    }
}
// Sticky nav bar ends
/////////////////////////////////////////

// Highlight nav bar starts
/////////////////////////////////////////
function highlight() {
    placePos = document.getElementById("P").offsetTop-navbar.offsetHeight;
    foodPos = document.getElementById("F").offsetTop-navbar.offsetHeight;
    sportPos = document.getElementById("S").offsetTop-navbar.offsetHeight;
    var current = document.getElementsByClassName("active");
    if (current.length > 0) {
        current[0].className = current[0].className.replace("active", "");
    }
    if (window.pageYOffset>=placePos && window.pageYOffset<foodPos){
        place.className+= "active";
    }
    if (window.pageYOffset>=foodPos && window.pageYOffset<sportPos){
        food.className+="active";
    }
    if (window.pageYOffset>=sportPos){
        sport.className+="active";
    }
}
// Highlight nav bar ends
/////////////////////////////////////////



// The modal starts
/////////////////////////////////////////
// Get the corresponding modal
var modal1 = document.getElementById('myModal1');
var modal2 = document.getElementById('myModal2');
var modal3 = document.getElementById('myModal3');
// Get the corresponding box
var box1 = document.getElementById('col1');
var box2 = document.getElementById('col2');
var box3 = document.getElementById('col3');
// Get the corresponding caption
var captionText1 = document.getElementById("caption1");
var captionText2 = document.getElementById("caption2");
var captionText3 = document.getElementById("caption3");

box1.onclick = function(){
    modal1.style.display = "block";
    captionText1.innerHTML = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
        "Suspendisse malesuada maximus mauris, quis egestas est tempus at. In in metus sem. " +
        "Pellentesque semper eros ac nisi egestas viverra. Aenean efficitur quam nulla. " +
        "Vestibulum condimentum ac nibh sed feugiat. Integer ut porta mauris. Sed vel nulla magna. " +
        "Vestibulum malesuada vitae ex nec varius. Nullam eget eros ornare, tristique nunc nec, vestibulum tellus.";
}
box2.onclick = function(){
    modal2.style.display = "block";
    captionText2.innerHTML = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
        "Suspendisse malesuada maximus mauris, quis egestas est tempus at. In in metus sem. " +
        "Pellentesque semper eros ac nisi egestas viverra. Aenean efficitur quam nulla. " +
        "Vestibulum condimentum ac nibh sed feugiat. Integer ut porta mauris. Sed vel nulla magna. " +
        "Vestibulum malesuada vitae ex nec varius. Nullam eget eros ornare, tristique nunc nec, vestibulum tellus.";
}
box3.onclick = function(){
    modal3.style.display = "block";
    captionText3.innerHTML = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
        "Suspendisse malesuada maximus mauris, quis egestas est tempus at. In in metus sem. " +
        "Pellentesque semper eros ac nisi egestas viverra. Aenean efficitur quam nulla. " +
        "Vestibulum condimentum ac nibh sed feugiat. Integer ut porta mauris. Sed vel nulla magna. " +
        "Vestibulum malesuada vitae ex nec varius. Nullam eget eros ornare, tristique nunc nec, vestibulum tellus.";
}
// Get the <span> element that closes the modal
var span1 = document.getElementById("close1");
var span2 = document.getElementById("close2");
var span3 = document.getElementById("close3");

// When the user clicks on <span> (x), close the modal
span1.onclick = function() {
    modal1.style.display = "none";
}
// When the user clicks on <span> (x), close the modal
span2.onclick = function() {
    modal2.style.display = "none";
}
// When the user clicks on <span> (x), close the modal
span3.onclick = function() {
    modal3.style.display = "none";
}
// The modal ends
/////////////////////////////////////////


////////////////////////////////////////
//Places starts
function displayDate() {
    console.log("Hello World!");
}

var slideIndex = 1;
showSlides(slideIndex);

window.plusSlides = function(n) {
    showSlides(slideIndex += n);
}

window.currentSlide = function(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) {slideIndex = 1}
    if (n < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className += " active";
}
//Places ends
////////////////////////////////////////

////////////////////////////////////////
//Sports starts
// Get the video
var video = document.getElementById("myVideo");

// Get the button
var btn = document.getElementById("myBtn");

// Pause and play the video, and change the button text
window.videoFunction = function() {
    if (video.paused) {
        video.play();
        btn.innerHTML = "Pause";
    } else {
        video.pause();
        btn.innerHTML = "Play";
    }
}
